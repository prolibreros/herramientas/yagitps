# encoding: UTF-8
# coding: UTF-8

require 'i18n'
require 'yaml'
require 'fileutils'
require __dir__ + '/md2po'
require __dir__ + '/po2md'

def check_input condition, err_msg
  if condition
    puts 'ERROR: ' + err_msg +
         " For help use '#{File.basename($0.to_s, '.*')} -h'."
    abort
  end
end

def gen_dirs path
  def gen path, dirs
    dirs.each do |d|
      if File.exist?(path + d) == false
        Dir.mkdir(path + d)
      end
    end
  end
  path  = path[-1] == '/' ? path : path + '/'
  langs = get_langs
  root  = get_root
  gen(root + path, langs)
  langs.each{|l| gen(root + path + l + '/', get_md_dirs)}
end

def gen_sha1_file sum, path, src = 'box'
  file_path = gen_sha1_file_path(sum, src)
  file = File.open(file_path, 'w:utf-8')
  file.puts sum.to_s + '  ' + path
  file.close
end

def gen_sha1_file_path sum, src
  base = src == 'box' ? 'box/etc/sha1/' : 'out/sha1/'
  return get_root + base + sum.to_s + '.sha1'
end

def get_argv arg, arr = false
  if ARGV.include? arg
    i = ARGV.find_index(arg) + 1
    if ARGV[i] != nil
      return arr ? ARGV[i].split(',') : ARGV[i].to_s
    else
      return nil
    end
  end
end

def get_config
  return YAML.load_file(get_root + 'in/config.yml')
end

def get_langs
  return get_config['general']['langs']
end

def get_md path
  return File.read(path)
             .split(/\n{2,}/)
             .map{|b| b.strip}
             .reject{|b| b.empty?}
end

def get_md_dirs
  return Dir.glob(get_root + 'in/txt/md/*')
            .select{|f| File.directory?(f)}
            .map{|f| File.basename(f)}
end

def get_output_dir i
  out = [
    'in/txt/po',
    'out/md',
    'out/html',
    'out/epub',
    'out/mobi',
    'out/tex',
    'out/pdf',
    'out/web',
  ]
  return out[i]
end

def get_po path
  po = []
  File.read(path).split(/\n{2,}/).each do |b|
    b = b.split(/msg\w{2,3}\s""\s*/)
         .reject{|e| e.empty?}
         .map{|e| e.strip.gsub(/^"(.*)"$/, '\1')
                         .gsub('\n', '')}
    po.push({:msgid => b[0], :msgstr => b[1]})
  end
  return po
end

def get_po_block block
  body  = []
  block = block.split(/\n+/)

  block.each_with_index do |l, i|
    l = '"' + l.strip

    # Differs ending if is a list or a block quote
    if l =~ /^"([\*\-\+]|\d.|>)\s+/
      l = l + '\n"'
    else
      l = l + ' "'
    end

    # Removes space in final line
    if i == block.length - 1
      l.gsub!(/\s+"/, '"')
    end

    # Cuts line if is greater than 80 chars
    if l.length > 80
      l = l[0..80] + "\"\n\"" + l[81..-1]
      l.gsub!(/"\n"$/, "\n")
    end

    body.push(l + "\n")
  end

  return body.join('').strip
end

def get_root
  return File.absolute_path(__dir__ + '/../..') + '/'
end

def get_sha1 sum, src = 'box'
  file_path = gen_sha1_file_path(sum, src)
  if File.exists? file_path
    els = File.read(file_path).strip.split('  ')
    return {:sha1 => els.first, :path => els[1..-1].join('  ')}
  else
    return nil
  end
end

def load_msg
  lang = get_langs.first
  path = get_root + 'box/etc/locales/'
  I18n.load_path << Dir[path + '*.yml']
  if File.exist?(path + lang + '.yml')
    I18n.default_locale = lang.to_sym
  end
end

def make step, files
  out = get_output_dir(step)
  ext = File.basename(out)
  cfg = get_config
  act = cfg[ext] ? cfg[ext]['active'] : true

  # Generates files if active
  if act
    print_msg(('make_' + ext).to_sym)
    if ext != 'web' then gen_dirs(out) end
    files.each do |i|
      send('make_' + ext, i)
    end
  end

  # Calls to next step
  if step > 0 && get_output_dir(step + 1)
    files.map!{|f| f.gsub(/in\/txt\/\w+/, 'out/md').gsub(/\w+$/, 'md')}
    make(step + 1, files)
  end
end

def make_epub input

end

def make_html input
  check_md(input)
end

def check_md md
  if !File.exist?(md.gsub(/in\/txt\/md/, 'out/md'))
    make_md(md, true)
  end
end 

def make_md input, just_copy = false
  output = input.gsub(/in\/txt\/\w+/, 'out/md')
                .gsub(/\.\w+$/, '.md')

  # Generates dir tree and copy files when is only one lang
  if just_copy
    gen_dirs('out/md')
    FileUtils.cp(input, output.gsub(/(out\/md)/, '\1' + '/' + get_langs[0]))
  else
    PO2MD.new(input).save(output)
  end

  # TODO: biber y notas
end

def make_mobi input

end

def make_pdf input

end

def make_po input
  get_langs.each_with_index do |l, index|
    output = input.gsub('in/txt/md', 'in/txt/po/' + l)
                  .gsub(/\.md$/, '.po')
    c = index == 0 ? true : false
    MD2PO.new(input, l, c).save(output)
  end
end

def make_tex input

end

def make_web input

end

def print_msg msg
  if $v then puts I18n.t(msg) end
end
